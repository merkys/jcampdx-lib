package org.jcamp.spectrum.notes;

/**
 * Insert the type's description here.
 * @author Thomas Weber
 */
public class BadContentException extends Exception {
    /**
     * BadContentException constructor comment.
     */
    public BadContentException() {
        super();
    }
    /**
     * BadContentException constructor comment.
     * @param s java.lang.String
     */
    public BadContentException(String s) {
        super(s);
    }
}
