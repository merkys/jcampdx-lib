package org.jcamp.math;

/**
 * special exception in data handling.
 * @author Thomas Weber
 */
public class DataException extends Exception {
    /**
     * DataException constructor comment.
     */
    public DataException() {
        super();
    }
    /**
     * DataException constructor comment.
     * @param s java.lang.String
     */
    public DataException(String s) {
        super(s);
    }
}
