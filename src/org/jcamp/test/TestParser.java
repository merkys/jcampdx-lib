package org.jcamp.test;

import java.io.BufferedReader;
import java.io.FileReader;

import junit.framework.TestCase;

import org.jcamp.parser.JCAMPReader;
import org.jcamp.spectrum.MassSpectrum;
import org.jcamp.spectrum.NMRSpectrum;
import org.jcamp.spectrum.Spectrum;

public class TestParser extends TestCase{
    public void testSpinworks() throws Exception{

        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader("testdata/spinworks.dx"));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();


        Spectrum jcampSpectrum = JCAMPReader.getInstance().createSpectrum(fileData.toString());
        if (!(jcampSpectrum instanceof NMRSpectrum)) {
        	throw new Exception("Spectrum in file is not an NMR spectrum!");
        }
        NMRSpectrum nmrspectrum = (NMRSpectrum) jcampSpectrum;
        if (nmrspectrum.hasPeakTable()) {
        	assertEquals(nmrspectrum.getPeakTable().length,16384);
        }
    }

    public void testMoreThan49Peaks() throws Exception{

        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader("testdata/1567755.jdx"));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();


        Spectrum jcampSpectrum = JCAMPReader.getInstance().createSpectrum(fileData.toString());
        if (!(jcampSpectrum instanceof MassSpectrum)) {
        	throw new Exception("Spectrum in file is not an NMR spectrum!");
        }
        MassSpectrum massspectrum = (MassSpectrum) jcampSpectrum;
        if (massspectrum.hasPeakTable()) {
        	assertEquals(massspectrum.getPeakTable().length,54);
        }
    }
}
