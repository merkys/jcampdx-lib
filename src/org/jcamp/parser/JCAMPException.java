package org.jcamp.parser;

/**
 * exceptions thrown at JCAMP-DX parsing.
 * @author Thomas Weber
 */
public class JCAMPException extends Exception {
    /**
     * JCAMPException constructor comment.
     */
    public JCAMPException() {
        super();
    }
    /**
     * JCAMPException constructor comment.
     * @param s java.lang.String
     */
    public JCAMPException(String s) {
        super(s);
    }
}
