package org.jcamp.parser;

import org.apache.regexp.RE;
import org.apache.regexp.RECompiler;
import org.apache.regexp.REProgram;
import org.apache.regexp.RESyntaxException;
/**
 * Iterator over JCAMP labels.
 * @author Thomas Weber
 */
public class LabelIterator implements IStringIterator {
	//shk3: there was a ^at the start of the regular expression, which lead
	//to only the first label being recognized
    private static String labelRegExp = "##[^=\\n\\r]*=";
    private static RECompiler compiler = new RECompiler();
    private REProgram labelProgram = null;
    {
        try {
            labelProgram = compiler.compile(labelRegExp);
        } catch (RESyntaxException e) {
            e.printStackTrace();
        }
    }
    private RE labelRE = new RE(labelProgram, RE.MATCH_CASEINDEPENDENT);
    private int offset = 0;
    private String jcamp;
    /**
    * LabelIterator constructor comment.
    */
    public LabelIterator(String jcamp) {
        super();
        this.jcamp = jcamp;
    }
    /**
     * @see com.creon.chem.jcamp.IStringIterator
     */
    public int getOffset() {
        if (jcamp == null)
            return -1;
        if (labelRE.match(jcamp, offset))
            return labelRE.getParenStart(0);
        else
            return -1;

    }
    /**
     * @see com.creon.chem.jcamp.IStringIterator
     */
    public boolean hasNext() {
        if (jcamp == null)
            return false;
        return labelRE.match(jcamp, offset);
    }
    /**
     * @see com.creon.chem.jcamp.IStringIterator
     */
    public String next() {
        if (jcamp == null)
            return null;
        int startMatch = offset;
        int endMatch = 0;
        String match;
        if (labelRE.match(jcamp, startMatch)) {
            match = labelRE.getParen(0);
            startMatch = labelRE.getParenStart(0);
            endMatch = labelRE.getParenEnd(0);
            offset = endMatch + 1;
            return match;
        } else
            return null;
    }
}
